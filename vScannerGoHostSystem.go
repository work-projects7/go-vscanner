package govscanner

import (
	"errors"
	"fmt"
	"strings"

	"github.com/vmware/govmomi/vim25/types"

	"github.com/vmware/govmomi/vim25/mo"
)

type HostCluster struct {
	HostName    string
	ClusterName string
}

// GetMoHosts returns a slice of all HostSystems on the vCenter loaded with the properties passed.
func (client *VMWareClient) GetMoHostSystems(properties []string) ([]mo.HostSystem, error) {
	var items []mo.HostSystem
	if err := client.RetrieveManagedObjects("HostSystem", properties, &items); err != nil {
		return nil, err
	}
	return items, nil
}

// GetMoHostSystemByName returns a HostSystem on the vCenter loaded with the properties passed the same name as the passed name string.
func (client *VMWareClient) GetMoHostSystemByName(host_name string, properties []string) (*mo.HostSystem, error) {
	var hostSystems []mo.HostSystem
	var items *mo.HostSystem = nil
	hostSystems, err := client.GetMoHostSystems(properties)
	if err != nil {
		return nil, err
	}
	for i := 0; i < len(hostSystems); i++ {
		if strings.EqualFold(hostSystems[i].Name, host_name) {
			items = &hostSystems[i]
		}
	}
	return items, nil
}

// GetHostSystemVMHBAs returns all FibreChannelHbas that are associated with the passed HostSystem.
func (client *VMWareClient) GetHostSystemVMHBAs(mo_host *mo.HostSystem) ([]types.BaseHostHostBusAdapter, error) {
	if mo_host.Config == nil {
		return nil, errors.New("mo_host.Config was nil, make sure this properties is loaded")
	} else if mo_host.Config.StorageDevice == nil {
		return nil, errors.New("mo_host.Config.StorageDevice was nil, make sure this properties is loaded")
	} else if mo_host.Config.StorageDevice.HostBusAdapter == nil {
		return nil, errors.New("mo_host.Config.StorageDevice.HostBusAdapter was nil, make sure this properties is loaded")
	}
	adapters := mo_host.Config.StorageDevice.HostBusAdapter
	if adapters == nil {
		return nil, errors.New("mo_host.Config.StorageDevice.HostBusAdapter was nil")
	}

	curatedAdapters := make([]types.BaseHostHostBusAdapter, 0)
	for _, adapter := range adapters {
		hhba := adapter.GetHostHostBusAdapter()
		if strings.Contains(hhba.Key, "FibreChannelHba") {
			curatedAdapters = append(curatedAdapters, adapter)
		}
	}

	return curatedAdapters, nil
}

// GetHostSystemPWWNs returns a list of all pwwns on the HostSystem.
func (client *VMWareClient) GetHostSystemPWWNs(mo_host *mo.HostSystem) ([]string, error) {
	adapters, err := client.GetHostSystemVMHBAs(mo_host)
	if err != nil {
		return nil, err
	}
	pwwns := make([]string, 0)
	for _, adapter := range adapters {
		pwwn := adapter.(*types.HostFibreChannelHba).PortWorldWideName
		pwwns = append(pwwns, fmt.Sprintf("%x", pwwn))
	}
	return pwwns, nil
}

// GetHostClusterNameArr returns an array of HostCluster struct's containing the host name and associated cluster name
func (client *VMWareClient) GetHostClusterNameArr(properties []string) ([]HostCluster, error) {
	items, err := client.GetMoHostSystems(properties)
	if err != nil {
		return nil, err
	}

	var hostClusterArr []HostCluster
	var parentObj mo.ClusterComputeResource
	for _, host := range items {
		parent := host.Parent
		if parent.Type == "ClusterComputeResource" {
			err := client.GetManagedObjectReferenceAttributes(host.Parent, []string{"name"}, &parentObj)
			if err != nil {
				return nil, err
			}
			parseName := strings.Split(host.Name, ".")
			data := HostCluster{
				HostName:    parseName[0],
				ClusterName: parentObj.Name,
			}
			hostClusterArr = append(hostClusterArr, data)
		}
	}
	return hostClusterArr, nil
}
