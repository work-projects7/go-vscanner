package govscanner_test

import (
	"context"
	"fmt"
	"net/url"
	"testing"

	"github.com/vmware/govmomi"
	"github.com/vmware/govmomi/object"
	"github.com/vmware/govmomi/simulator"
	"github.com/vmware/govmomi/simulator/vpx"
	"github.com/vmware/govmomi/view"
	"github.com/vmware/govmomi/vim25/types"
	govscanner "gitlab.com/work-projects7/go-vscanner"
)

func TestHostClusterName(t *testing.T) {
	ctx := context.Background()

	govmomiClient, ts, err := simVcenter(ctx)
	if err != nil {
		t.Fatal(err)
	}
	defer ts.Close()

	ottClient := govscanner.NewVMWareClient(ctx, "test", govmomiClient, view.NewManager(govmomiClient.Client))

	clusterName, err := ottClient.GetHostClusterNameArr([]string{})
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println("clusterName: ", clusterName)
}

// simulates a vcenter with a cluster and a host
func simVcenter(ctx context.Context) (*govmomi.Client, *simulator.Server, error) {
	content := vpx.ServiceContent
	s := simulator.New(simulator.NewServiceInstance(content, vpx.RootFolder))

	ts := s.NewServer()

	c, err := govmomi.NewClient(ctx, ts.URL, true)
	if err != nil {
		return nil, nil, err
	}

	f := object.NewRootFolder(c.Client)

	dc, err := f.CreateDatacenter(ctx, "foo")
	if err != nil {
		return nil, nil, err
	}

	folders, err := dc.Folders(ctx)
	if err != nil {
		return nil, nil, err
	}

	cluster, err := folders.HostFolder.CreateCluster(ctx, "cluster1", types.ClusterConfigSpecEx{})
	if err != nil {
		return nil, nil, err
	}

	spec := types.HostConnectSpec{}
	spec.HostName = "localhost"

	task, err := cluster.AddHost(ctx, spec, true, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	_, err = task.WaitForResult(ctx, nil)
	if err != nil {
		return nil, nil, err
	}
	return c, ts, nil
}

func BenchmarkUnfilteredSpeed(b *testing.B) {
	vCenterContext, cancel := context.WithCancel(context.Background())
	defer cancel()

	directURL, err := url.Parse("@bn-vcenter.uplinkdata.com/sdk")
	if err != nil {
		b.Fatalf("Error parsing url %s\n", err.Error())
		return
	}

	govmomiClient, err := govmomi.NewClient(vCenterContext, directURL, true)
	if err != nil {
		b.Fatalf("Logging in error: %s\n", err.Error())
		return
	}

	ottClient := govscanner.NewVMWareClient(vCenterContext, "test", govmomiClient, view.NewManager(govmomiClient.Client))

	err1 := govmomiClient.Logout(vCenterContext)
	if err1 != nil {
		b.Fatalf("Error logging out of VM: %s\n", err1.Error())
	}

	var foundVM, err2 = ottClient.GetMoVirtualMachineByName("chedl-netbox2", nil)
	if err2 != nil {
		b.Fatalf("Error finding VM: %s\n", err2.Error())
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err = ottClient.GetVirtualMachineClusterName(foundVM)
		if err != nil {
			b.Fatalf("Error getting Virtual Machine Cluster Name %s\n", err.Error())
		}
	}
}
