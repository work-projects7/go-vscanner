// Package govscanner is a vScanner module rewrite for goLang. Initial code provided by Timothy White and extended by Adam Marr.
package govscanner

import (
	"context"
	"log"

	"github.com/vmware/govmomi/event"

	"github.com/vmware/govmomi"
	"github.com/vmware/govmomi/property"
	"github.com/vmware/govmomi/view"
	"github.com/vmware/govmomi/vim25/mo"
	"github.com/vmware/govmomi/vim25/types"
)

// VMWareClient is a data structure used to contain commonly used attributes.
type VMWareClient struct {
	VcenterName string
	ctx         context.Context
	Cancel      context.CancelFunc
	vmclient    *govmomi.Client
	manager     *view.Manager
}

// NewVMWareClient Is a constructor that initializes attributes for an VMWareClient.
func NewVMWareClient(context context.Context, vCenterName string, clientIn *govmomi.Client, managerIn *view.Manager) *VMWareClient {
	client := new(VMWareClient)
	client.VcenterName = vCenterName
	client.ctx = context
	client.vmclient = clientIn
	client.manager = managerIn
	return client
}

// GetAllManagedObjectReferenceAttributes loads the given attributes of the given slice of ManagedObjectReference and stores it into the given dataDestination.
func (client *VMWareClient) GetAllManagedObjectReferenceAttributes(moRefs *[]types.ManagedObjectReference, attributeNames []string, dataDestination interface{}) error {
	pc := property.DefaultCollector(client.vmclient.Client)
	err := pc.Retrieve(client.ctx, *moRefs, attributeNames, dataDestination)
	return err
}

// GetEventHandlerForReferences takes a slice of ManagedObjectReference and returns an eventManager that handles those events.
func (client *VMWareClient) GetEventHandlerForReferences(pageSize int32, tail bool, references []types.ManagedObjectReference, handlerFunction func(types.ManagedObjectReference, []types.BaseEvent) error) error {
	if len(references) > 10 {
		log.Printf("WARNING: Number of references to gather events from greater than 10: %v\n", len(references))
	}
	eventManager := event.NewManager(client.vmclient.Client)
	err := eventManager.Events(client.ctx, references, pageSize, tail, true, handlerFunction)
	if err != nil {
		return err
	}
	return nil
}

// GetManagedObjectReferenceAttribute loads the given attribute of the given ManagedObjectReference and stores it into the given dataDestination.
func (client *VMWareClient) GetManagedObjectReferenceAttributes(moRef *types.ManagedObjectReference, attributeNames []string, dataDestination interface{}) error {
	pc := property.DefaultCollector(client.vmclient.Client)
	err := pc.RetrieveOne(client.ctx, *moRef, attributeNames, dataDestination)
	return err
}

// GetMoFolders returns an array of folders.
func (client *VMWareClient) GetMoFolders() ([]mo.Folder, error) {
	var items []mo.Folder
	if err := client.RetrieveManagedObjects("Folder", nil, &items); err != nil {
		return nil, err
	}
	return items, nil
}

// GetMoNetworks returns an array of HostNetworkSystems.
func (client *VMWareClient) GetMoNetworks(properties []string) ([]mo.HostNetworkSystem, error) {
	var items []mo.HostNetworkSystem
	if err := client.RetrieveManagedObjects("HostNetworkSystem", properties, &items); err != nil {
		return nil, err
	}
	return items, nil
}

// RetrieveManagedObjects Creates a ContainerView and Retrieves items that match managedObjectType and put them into dst must be a list of govmomi/vim25/mo.
func (client *VMWareClient) RetrieveManagedObjects(managedObjectType string, properties []string, dst interface{}) (err error) {
	managedObjectTypes := []string{managedObjectType}
	v, err := client.manager.CreateContainerView(client.ctx, client.vmclient.Client.ServiceContent.RootFolder, managedObjectTypes, true)
	if err != nil {
		return err
	}
	defer func() {
		err = v.Destroy(client.ctx)
		if err != nil {
			log.Print("Error on close client")
		}
	}()

	if err = v.Retrieve(client.ctx, managedObjectTypes, properties, dst); err != nil {
		return err
	}
	return nil
}
