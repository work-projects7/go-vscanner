package govscanner

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/vmware/govmomi/object"
	"github.com/vmware/govmomi/vim25/mo"
	"github.com/vmware/govmomi/vim25/types"
)

// DetachVMDevice disconnects a given device from a given vm.
func (client *VMWareClient) DetachVMDevice(vm_obj *object.VirtualMachine, device *types.BaseVirtualDevice) error {
	deviceList, err := vm_obj.Device(client.ctx)
	if err != nil {
		return err
	}
	err = deviceList.Disconnect(*device)
	if err != nil {
		return err
	}
	err = vm_obj.EditDevice(client.ctx, *device)
	if err != nil {
		return err
	}
	return nil
}

// EjectISO ejects the iso from a given cdRom and vm and a cdRom object. This will often prompt a question which can be answered with AnswerVM.
func (client *VMWareClient) EjectISO(vm_obj *object.VirtualMachine, cdRom *types.VirtualCdrom) (*object.Task, error) {
	deviceList, err := vm_obj.Device(client.ctx)
	if err != nil {
		return nil, err
	}
	cdBack := deviceList.EjectIso(cdRom)

	spec := types.VirtualMachineConfigSpec{}
	config := &types.VirtualDeviceConfigSpec{
		Device:    cdBack,
		Operation: types.VirtualDeviceConfigSpecOperationEdit,
	}
	spec.DeviceChange = append(spec.DeviceChange, config)
	result, err := (*vm_obj).Reconfigure(client.ctx, spec)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// AnswerVM Answers a VM question with the given response.
func (client *VMWareClient) AnswerVM(vm_obj *object.VirtualMachine, id string, response string) error {
	err := (*vm_obj).Answer(client.ctx, id, response)
	if err != nil {
		return err
	}
	return nil
}

// GetCommonVMFromMO returns a given common virtual machine object representation of the managed object virtual machine. This object can be used to perform actions on the given VM.
func (client *VMWareClient) GetCommonVMFromMO(vmObject mo.VirtualMachine) (*object.VirtualMachine, error) {
	commonVM := object.NewVirtualMachine(client.vmclient.Client, vmObject.Reference())
	return commonVM, nil
}

// GetMoVirtualMachines returns all found virtual machines on a vCenter as a []VirtualMachines.
func (client *VMWareClient) GetMoVirtualMachines(properties []string) ([]mo.VirtualMachine, error) {
	var items []mo.VirtualMachine
	if err := client.RetrieveManagedObjects("VirtualMachine", properties, &items); err != nil {
		return nil, err
	}
	return items, nil
}

// GetMoVirtualMachineByName finds and returns a VirtualMachine object with the given name. properties should contain "config.name" as one of the entries.
func (client *VMWareClient) GetMoVirtualMachineByName(vmName string, properties []string) (*mo.VirtualMachine, error) {
	var virtualMachines []mo.VirtualMachine
	var items *mo.VirtualMachine = nil
	virtualMachines, err := client.GetMoVirtualMachines(properties)
	if err != nil {
		return nil, err
	}
	for i := 0; i < len(virtualMachines); i++ {
		if virtualMachines[i].Config != nil {
			if strings.EqualFold(virtualMachines[i].Config.Name, vmName) {
				items = &virtualMachines[i]
			}
		} else {
			return nil, errors.New("vm.config was nil, ensure that config.name is one of the properties passed as an argument")
		}
	}
	return items, nil
}

// GetVirtualMachineClusterName takes a vm reference and returns the name of the cluster it belongs to as a string.
func (client *VMWareClient) GetVirtualMachineClusterName(vmRef *mo.VirtualMachine) (string, error) {
	var hostObj mo.ManagedEntity
	var err error

	if vmRef.Summary.Runtime.Host != nil {
		if vmRef.Summary.Runtime.Host.Type == "HostSystem" {
			err = client.GetManagedObjectReferenceAttributes(vmRef.Summary.Runtime.Host, []string{"parent"}, &hostObj)
		} else {
			//They gave us the wrong data type for the Runtime Host.
			err = errors.New("The provided vmRef.Summary.Runtime.Host is " + vmRef.Summary.Runtime.Host.Type + " expected type HostSystem")
		}
	} else {
		err = errors.New("the provided vmRef.Summary.Runtime is nil")
	}

	if err != nil {
		return "", err
	}

	var parentObj mo.ClusterComputeResource

	if hostObj.Parent != nil {
		if hostObj.Parent.Type == "ClusterComputeResource" {
			err = client.GetManagedObjectReferenceAttributes(hostObj.Parent, []string{"name"}, &parentObj)
		} else {
			//They gave us the wrong data type for the hostObj.Parent
			err = errors.New("The provided vmRef, hostObj.Parent type is " + hostObj.Parent.Type + " expected type ClusterComputeResource")
		}
	} else {
		err = errors.New("the provided vmRef, hostObj is nil")
	}

	if err != nil {
		return "", err
	}
	return parentObj.Name, nil
}

// GetVirtualMachineDiskThinlyProvisioned Returns whether the VM is thinly provisioned.
func (client *VMWareClient) GetVirtualMachineDiskThinlyProvisioned(disk *types.VirtualDisk) (bool, error) {
	if backing, ok := disk.Backing.(*types.VirtualDiskFlatVer2BackingInfo); ok {
		return *backing.ThinProvisioned, nil
	}
	return false, fmt.Errorf("unable to cast to get provisioned information")
}

// GetVirtualMachineSnapshotTree returns an array of VirtualMachineSnapshotTrees.
func (client *VMWareClient) GetVirtualMachineSnapshotTree(vmRef *mo.VirtualMachine) ([]types.VirtualMachineSnapshotTree, error) {
	var snapshotList []types.VirtualMachineSnapshotTree
	if vmRef.Snapshot == nil {
		return nil, nil
	}
	snapshotList = vmRef.Snapshot.RootSnapshotList
	return snapshotList, nil
}

// GetVirtualMachineSnapshotTreeAge returns a float64 representing the age of the snapshot in hours.
func (client *VMWareClient) GetVirtualMachineSnapshotTreeAge(snapTreeObj *types.VirtualMachineSnapshotTree) time.Duration {
	age := time.Since(snapTreeObj.CreateTime)
	return age
}

// GetVirtualMachineVirtualDisks returns a slice of BaseVirtualDevices that are VirtualDisks.
func (client *VMWareClient) GetVirtualMachineVirtualDisks(vm *mo.VirtualMachine) ([]*types.VirtualDisk, error) {
	//TODO error checking on attribute existing.
	var allDisks []*types.VirtualDisk
	for _, device := range (*vm).Config.Hardware.Device {
		switch deviceType := device.(type) {
		case *types.VirtualDisk:
			allDisks = append(allDisks, deviceType)
		}
	}
	return allDisks, nil
}

// RelocateVMtoDatastore moves a given VirtualMachine to a new datastore using the datastore reference. Makes sure that all disks are thinly provisioned on arrival.
func (client *VMWareClient) RelocateVMtoDatastore(vm_obj *object.VirtualMachine, datastore_ref *types.ManagedObjectReference, host_ref *types.ManagedObjectReference, pool_ref *types.ManagedObjectReference, newCluster bool) (*object.Task, error) {
	relocateSpec := types.VirtualMachineRelocateSpec{}
	relocateSpec.Datastore = datastore_ref
	if newCluster {
		relocateSpec.Host = host_ref
		relocateSpec.Pool = pool_ref
	}
	relocateSpec.Disk = []types.VirtualMachineRelocateSpecDiskLocator{}
	relocateDisk := types.VirtualMachineRelocateSpecDiskLocator{}
	relocateDisk.Datastore = *datastore_ref
	deviceList, err := vm_obj.Device(client.ctx)
	if err != nil {
		return nil, err
	}
	diskList := deviceList.SelectByType((*types.VirtualDisk)(nil))
	thin := true
	for _, disk := range diskList {
		vDisk := disk.(*types.VirtualDisk)
		vDiskBacking := vDisk.GetVirtualDevice().Backing
		adjustedBacking, ok := vDiskBacking.(*types.VirtualDiskFlatVer2BackingInfo)
		if !ok {
			return nil, errors.New("invalid assertion in RelocateVMtoDatastore")
		}
		adjustedBacking.ThinProvisioned = &thin
		relocateDisk.DiskId = vDisk.Key
		relocateDisk.DiskBackingInfo = adjustedBacking
		relocateSpec.Disk = append(relocateSpec.Disk, relocateDisk)
	}
	// relocateSpec.Disk = nil
	priority := types.VirtualMachineMovePriorityDefaultPriority
	task, err := vm_obj.Relocate(client.ctx, relocateSpec, priority)
	if err != nil {
		return nil, err
	}
	return task, nil
}

// GetMoResourcePools returns all found resource pools on a vCenter as a []ResourcePool.
func (client *VMWareClient) GetMoResourcePools(properties []string) ([]mo.ResourcePool, error) {
	var items []mo.ResourcePool
	if err := client.RetrieveManagedObjects("ResourcePool", properties, &items); err != nil {
		return nil, err
	}
	return items, nil
}
