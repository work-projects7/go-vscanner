package govscanner

import (
	"errors"
	"strings"

	"github.com/vmware/govmomi/object"
	"github.com/vmware/govmomi/vim25/mo"
	"github.com/vmware/govmomi/vim25/types"
)

// GetStoragePodVMs returns a slice of all virtual machines that belong to the datastore cluster/StoragePod
func (client *VMWareClient) GetStoragePodVMs(storagePod *mo.StoragePod, properties []string) ([]mo.VirtualMachine, error) {
	podVMs := make([]mo.VirtualMachine, 0)

	podDatastores, err := client.GetStoragePodDatastores(storagePod, []string{"vm"})
	if err != nil {
		return nil, err
	}

	for _, ds := range podDatastores {
		vms, err := client.GetDatastoreVirtualMachines(&ds, properties)
		if err != nil {
			return nil, err
		}
		podVMs = append(podVMs, vms...)
	}

	return podVMs, nil
}

// GetStoragePodDatastoreByName returns a slice of all datastores associated with a pod name
func (client *VMWareClient) GetStoragePodDatastoreByName(podName string) ([]mo.Datastore, error) {
	storagePod, err := client.GetStoragePodByName(podName, []string{})
	if err != nil {
		return nil, err
	}
	datastoreArr, err := client.GetStoragePodDatastores(storagePod, []string{})
	if err != nil {
		return nil, err
	}
	return datastoreArr, nil
}

// GetStoragePodDatastores returns a slice of all datastores associated with a given pod.
func (client *VMWareClient) GetStoragePodDatastores(storagePod *mo.StoragePod, properties []string) ([]mo.Datastore, error) {
	podDatastores := make([]mo.Datastore, 0)

	if len(storagePod.ChildEntity) > 0 {
		err := client.GetAllManagedObjectReferenceAttributes(&storagePod.ChildEntity, properties, &podDatastores)
		if err != nil {
			return nil, err
		}
	}

	return podDatastores, nil
}

// GetReccomendedVMDatastoreFromStoragePodName Requests a drs recommendation to relocate a vm to a storage pod. Returns a result that includes a recommended datastore and associated data.
func (client *VMWareClient) GetReccomendedVMDatastoreFromStoragePodName(podName string, host *mo.HostSystem, vmRef *types.ManagedObjectReference) (*types.StoragePlacementResult, error) {
	pod, err := client.GetStoragePodByName(podName, nil)
	if err != nil {
		return nil, err
	}
	podRef := pod.Reference()

	podSelectionSpec := types.StorageDrsPodSelectionSpec{
		StoragePod: &podRef,
	}

	var placementSpec types.StoragePlacementSpec

	if host != nil && host.Self.Value != "" {
		placementSpec = types.StoragePlacementSpec{
			Priority: types.VirtualMachineMovePriorityDefaultPriority,
			Vm:       vmRef,
			RelocateSpec: &types.VirtualMachineRelocateSpec{
				Host:         &host.Self,
				DiskMoveType: string(types.VirtualMachineRelocateDiskMoveOptionsMoveAllDiskBackingsAndAllowSharing),
			},
			PodSelectionSpec: podSelectionSpec,
			//Type:             string(types.StoragePlacementSpecPlacementTypeRelocate),
			Type: "relocate",
		}
	} else {
		placementSpec = types.StoragePlacementSpec{
			Priority: types.VirtualMachineMovePriorityDefaultPriority,
			Vm:       vmRef,
			RelocateSpec: &types.VirtualMachineRelocateSpec{
				//Host:         hostRef,
				DiskMoveType: string(types.VirtualMachineRelocateDiskMoveOptionsMoveAllDiskBackingsAndAllowSharing),
			},
			PodSelectionSpec: podSelectionSpec,
			//Type:             string(types.StoragePlacementSpecPlacementTypeRelocate),
			Type: "relocate",
		}
	}

	srm := object.NewStorageResourceManager(client.vmclient.Client)
	reccomendedDatastore, err := srm.RecommendDatastores(client.ctx, placementSpec)
	if err != nil {
		return nil, err
	}
	return reccomendedDatastore, nil
}

// GetStoragePodByName returns a StoragePod object that has the same name as the argument, lodead with all the values contained in properties.
func (client *VMWareClient) GetStoragePodByName(podName string, properties []string) (*mo.StoragePod, error) {
	var storagePods []mo.StoragePod
	var items *mo.StoragePod = nil
	storagePods, err := client.GetMOStoragePods(properties)
	if err != nil {
		return nil, err
	}
	for i := 0; i < len(storagePods); i++ {
		if storagePods[i].Name != "" {
			if strings.EqualFold(storagePods[i].Name, podName) {
				items = &storagePods[i]
			}
		} else {
			return nil, errors.New("storagePod.name was empty, ensure that name is one of the properties passed as an argument")
		}
	}
	return items, nil
}

// GetMOStoragePods returns a slice containing all StoragePods in the environment.
func (client *VMWareClient) GetMOStoragePods(properties []string) ([]mo.StoragePod, error) {
	var items []mo.StoragePod
	if err := client.RetrieveManagedObjects("StoragePod", properties, &items); err != nil {
		return nil, err
	}
	return items, nil
}
