package govscanner

import (
	"errors"
	"strings"

	"github.com/vmware/govmomi/vim25/mo"
	"github.com/vmware/govmomi/vim25/types"
)

// GetMoClusterCompute returns a []ClusterComputeResource representing all clusters that exist on the vCenter.
func (client *VMWareClient) GetMoClusterCompute(properties []string) ([]mo.ClusterComputeResource, error) {
	var items []mo.ClusterComputeResource
	if err := client.RetrieveManagedObjects("ClusterComputeResource", properties, &items); err != nil {
		return nil, err
	}
	return items, nil
}

// GetMoClusterComputeByName returns an mo.ClusterComputeResource with the same name as the passed string.
func (client *VMWareClient) GetMoClusterComputeByName(name string, properties []string) (*mo.ClusterComputeResource, error) {
	var clusters []mo.ClusterComputeResource
	if err := client.RetrieveManagedObjects("ClusterComputeResource", properties, &clusters); err != nil {
		return nil, err
	}
	for _, cluster := range clusters {
		if strings.EqualFold(cluster.Name, name) {
			return &cluster, nil
		}
	}
	return nil, nil
}

// GetClusterComputeDatastoreRefs returns a []ManagedObjectReference or an error if the attribute is nil.
func (client *VMWareClient) GetClusterComputeDatastoreRefs(clusterObject mo.ClusterComputeResource) ([]types.ManagedObjectReference, error) {
	if clusterObject.Datastore == nil {
		return nil, errors.New("GetClusterComputeDatastoreRefs found nil in clusterObject.Datastore, make sure the attribute is loaded")
	}
	datastoreReferences := clusterObject.Datastore
	return datastoreReferences, nil
}

// GetClusterComputeMoVirtualMachines takes a cluster object and []string of properties to load and returns a []mo.VirtualMachine loaded with the given properties.
func (client *VMWareClient) GetClusterComputeMoVirtualMachines(clusterObject *mo.ClusterComputeResource, properties []string) ([]mo.VirtualMachine, error) {
	vmRefs, err := client.GetClusterComputeVirtualMachinesReferences(clusterObject)
	if err != nil {
		return nil, err
	}
	var vmObjs []mo.VirtualMachine
	err = client.GetAllManagedObjectReferenceAttributes(&vmRefs, properties, &vmObjs)
	if err != nil {
		return nil, err
	}
	return vmObjs, nil
}

// GetClusterComputeVirtualMachines takes a cluster reference and returns a list of references to all VMs that belong to that cluster or an error.
func (client *VMWareClient) GetClusterComputeVirtualMachinesReferences(clusterObject *mo.ClusterComputeResource) ([]types.ManagedObjectReference, error) {
	clusterResourcePool := clusterObject.ResourcePool
	if clusterResourcePool == nil {
		return nil, errors.New("ClusterComputeResource.ResourcePool is nil, make sure the attribute is loaded")
	}
	var vmRefs mo.ResourcePool
	err := client.GetManagedObjectReferenceAttributes(clusterResourcePool, []string{"vm"}, &vmRefs)
	if err != nil {
		return nil, err
	}
	return vmRefs.Vm, nil
}
