module gitlab.com/work-projects7/go-vscanner

go 1.18

require github.com/vmware/govmomi v0.24.0

require github.com/google/uuid v0.0.0-20170306145142-6a5e28554805 // indirect
