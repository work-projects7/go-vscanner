package govscanner

import (
	"strings"

	"github.com/vmware/govmomi/vim25/mo"
)

// GetMoDatastores returns an array of Datastores.
func (client *VMWareClient) GetMoDatastores(properties []string) ([]mo.Datastore, error) {
	var items []mo.Datastore
	if err := client.RetrieveManagedObjects("Datastore", properties, &items); err != nil {
		return nil, err
	}
	return items, nil
}

// GetMoDatastoreByName returns a mo.Datastore that's name matches the input string. Takes a list of properties to load attributes.
func (client *VMWareClient) GetMoDatastoreByName(datastoreName string, properties []string) (*mo.Datastore, error) {
	var allDatastores []mo.Datastore
	var items *mo.Datastore = nil
	allDatastores, err := client.GetMoDatastores(properties)
	if err != nil {
		return nil, err
	}
	for i := 0; i < len(allDatastores); i++ {
		if strings.EqualFold(allDatastores[i].Name, datastoreName) {
			items = &allDatastores[i]
		}
	}
	return items, nil
}

// GetDatastoreVirtualMachines returns all virtual machines associated with a given datastore object.
func (client *VMWareClient) GetDatastoreVirtualMachines(datastore *mo.Datastore, properties []string) ([]mo.VirtualMachine, error) {
	var dsVMs []mo.VirtualMachine

	if len(datastore.Vm) > 0 {
		err := client.GetAllManagedObjectReferenceAttributes(&datastore.Vm, properties, &dsVMs)
		if err != nil {
			return nil, err
		}
	}

	return dsVMs, nil
}
